import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        numeroPaso: 0,
        nombre: "",
        apellido: "",
        dni: "",
    },
    mutations: {
        guardarNombre(state, nombre) {
            state.nombre = nombre;
        },
        guardarApellido(state, apellido) {
            state.apellido = apellido;
        },
        guardarDni(state, dni) {
            state.dni = dni;
        },
    },
    getters: {
        obtenerSaludoFinal(state) {
            return `Bienvenido ${state.nombre} ${state.apellido}, dni: ${state.dni} `
        }
    },
    actions: {
        pruebaAction(context) {
            setTimeout(() => {
                context.commit("guardarNombre", "hola");

            }, 10000);
        }
    },

});
